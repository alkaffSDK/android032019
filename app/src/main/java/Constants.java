public class Constants {

    public static final String APP_TAG = "android032019";
    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_RESULT = "result";
    public static final String SHARED_PREFRENCES_FILE = "MyPrefs";

    public  static final class Actions
    {
        public static final String MY_ACTION = "com.sdkjordan.a.alkaff.android032019.MY_ACTION";
    }


}
