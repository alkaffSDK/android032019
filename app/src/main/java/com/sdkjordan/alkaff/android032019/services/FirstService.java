package com.sdkjordan.alkaff.android032019.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import com.sdkjordan.alkaff.android032019.Constants;
import java.util.Locale;

public class FirstService  extends Service {

    public static final String TAG = "services" ;
    public static final String CLASS_NAME = "FirstService";
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "FirstService:"+ "onCreate()");
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, Thread.currentThread().getName()+"-FirstService:"+ "onStartCommand()");
        Log.i(TAG,Thread.currentThread().getName()+String.format(Locale.getDefault(),"- Data: %20s , Flags: %3d , StartID: %d",
                intent.getStringExtra(Constants.EXTRA_DATA),flags,startId));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, Thread.currentThread().getName()+"-FirstService:"+ "End working") ;
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        Log.d(TAG, "FirstService:"+ "onDestroy()") ;
        super.onDestroy();
    }
    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "FirstService:"+ "onUnbind()") ;
        return super.onUnbind(intent);
    }
    @Override
    public void onRebind(Intent intent) {
        Log.d(TAG, "FirstService:"+ "onRebind()") ;
        super.onRebind(intent);
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "FirstService:"+ "onBind()") ;
        return null;
    }
}
