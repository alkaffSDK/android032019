package com.sdkjordan.alkaff.android032019.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sdkjordan.alkaff.android032019.Constants;

public class StaticReceiver extends BroadcastReceiver {


    private String TAG = "Receiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        String data = "No data found!";
        if(intent != null )
        {
            data = intent.getStringExtra(Constants.EXTRA_DATA);
        }
        Log.d(TAG,"Static Receiver : Data:"+ data);
    }
}
