package com.sdkjordan.alkaff.android032019;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MyNewActivity extends AppCompatActivity {

    public static final String TAG = "android032019";
    private static final String ACTIVITY = "MyNewActivity";

    private  String mResultData ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mynew);
        Log.d(TAG, ACTIVITY + ": onCreate()");

        inti();

        Intent intent = getIntent() ;
        String data = intent.getStringExtra(Constants.EXTRA_DATA) ;

        if(null != data && ! data.isEmpty()) {
            mEditText2.setText(data);
            mResultData = data ;
        }
        else {
            mEditText2.setHint(getResources().getString(R.string.txt_noData));
            setResult(RESULT_CANCELED);
        }
    }

    private EditText mEditText2 ;
    private void inti() {
        mEditText2 = findViewById(R.id.editText2);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, ACTIVITY + ": onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, ACTIVITY + ": onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, ACTIVITY + ": onDestroy()");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, ACTIVITY + ": onBackPressed()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, ACTIVITY + ": onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, ACTIVITY + ": onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, ACTIVITY + ": onRestart()");
    }

    public  void  onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.buttonAction:
                doAction(); break;
            case R.id.buttonFinish:

                if(mResultData != null)
                {
                   mResultData =  doActionOnData(mResultData) ;
                   Intent resultIntent = new Intent();
                   resultIntent.putExtra(Constants.EXTRA_RESULT,mResultData);
                   setResult(RESULT_OK,resultIntent);
                }
                finish();
                break;
        }
    }

    private String doActionOnData(String mResultData) {

        return  mResultData.toUpperCase();
    }

    private void doAction() {
    }
}
