package com.sdkjordan.alkaff.android032019.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.Nullable;
import com.sdkjordan.alkaff.android032019.Constants;
import java.util.Locale;

public class SecondIntentService extends IntentService {

    public static final String TAG = "services" ;
    public static final String CLASS_NAME = "SecondIntentService" ;

    public SecondIntentService() {
        this(CLASS_NAME);
    }
    public SecondIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d(TAG, Thread.currentThread().getName()+"-SecondIntentService:"+ "onHandleIntent()") ;
        Log.i(TAG,Thread.currentThread().getName()+String.format(Locale.getDefault(),"- Data: %20s ",
                intent.getStringExtra(Constants.EXTRA_DATA)));
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, Thread.currentThread().getName()+"-SecondIntentService:"+ "End working") ;
    }
}
