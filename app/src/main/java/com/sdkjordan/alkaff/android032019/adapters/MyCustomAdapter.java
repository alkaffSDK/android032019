package com.sdkjordan.alkaff.android032019.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.sdkjordan.alkaff.android032019.R;
import java.util.Random;

public class MyCustomAdapter extends BaseAdapter {

    private Context mContext ;
    private Random random = new Random();

    private String[] data ;
    public MyCustomAdapter(Context context) {
        mContext =context ;
        data = mContext.getResources().getStringArray(R.array.countries);
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public String getItem(int position) {
        if (position < 0 || position > data.length - 1)
            return  null ;

        return data[position];
    }

    @Override
    public long getItemId(int position) {
        if (position < 0 || position > data.length - 1)
            return  -1 ;
        return  position ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.list_items_layout,null);
        TextView textView = view.findViewById(R.id.textViewName);
        textView.setText(getItem(position));
        textView.setTextColor(Color.argb(random.nextInt(255),random.nextInt(255),random.nextInt(255),random.nextInt(255)));
        return view;
    }
}
