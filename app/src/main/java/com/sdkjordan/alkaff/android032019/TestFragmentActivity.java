package com.sdkjordan.alkaff.android032019;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.sdkjordan.alkaff.android032019.R;
import com.sdkjordan.alkaff.android032019.fragments.MyDetailsFragment;
import com.sdkjordan.alkaff.android032019.fragments.MyListFragment;

public class TestFragmentActivity extends AppCompatActivity implements MyListFragment.ListFragmentInterface {

    String[] data ;

    FragmentManager fragmentManager ;
    MyDetailsFragment myDetailsFragment ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragement);
        data = getResources().getStringArray(R.array.countries);

        fragmentManager =  getSupportFragmentManager() ;

        myDetailsFragment = (MyDetailsFragment) fragmentManager.findFragmentById(R.id.fragment_details);
    }

    @Override
    public String[] getData() {
        return data;
    }

    @Override
    public void selectChanged(int id) {

        StringBuilder  sb = new StringBuilder(data[id]);
        String reverse = sb.reverse().toString() ;
        myDetailsFragment.changeContent(id,data[id],reverse );
    }
}
