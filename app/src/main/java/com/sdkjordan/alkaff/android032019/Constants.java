package com.sdkjordan.alkaff.android032019;

public class Constants {

    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_RESULT = "result";

    public static class Actions {
        public static final String FIRST_ACTION = "com.sdkjordan.alkaff.android032019.myAction";
        public static final String MY_ACTION = "com.sdkjordan.a.alkaff.android032019.MY_ACTION";

    }




    public  static final class Keys {
        public static final String KEY_NAME =  "name";
        public static final String KEY_PHONE =  "phone";
    }
}
