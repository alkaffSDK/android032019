package com.sdkjordan.alkaff.android032019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sdkjordan.alkaff.android032019.helpers.MyDatabaseHelper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {


    private static final String TAG = "android032019";
    private static String[] from = {Constants.Keys.KEY_NAME, Constants.Keys.KEY_PHONE};
    private int[] to = {android.R.id.text1, android.R.id.text2};
    private int[] to2 = {R.id.textViewName, R.id.editTextPhone};

    List<Map<String, String>> contacts = new ArrayList<>();

    SimpleAdapter adapter;
    private String FileName = "data.txt";

    private SharedPreferences mSharedPreferences ;


    private MyDatabaseHelper myDatabaseHelper ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        // To read/write in the cache directory
        // FileName = getCacheDir() + "/"+FileName ;
        inti();
        printExternalData();
        String[] data = {"A", "B", "C"};

        data = getResources().getStringArray(R.array.countries);
//      ListAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,data);

//      ListAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_2,android.R.id.text1,data);
        // ListAdapter adapter = new ArrayAdapter<String>(this,R.layout.list_items_layout,R.id.textViewName,data);

//        HashMap<String,String> map = new HashMap<>();
//        map.put(Constants.Keys.KEY_NAME,"Ahmed"); map.put(Constants.Keys.KEY_PHONE,"0788686870");
//
//        HashMap<String,String> map1 = new HashMap<>();
//        map1.put(Constants.Keys.KEY_NAME,"Abed"); map1.put(Constants.Keys.KEY_PHONE,"0785447514");
//
//        HashMap<String,String> map2 = new HashMap<>();
//        map2.put(Constants.Keys.KEY_NAME,"Emad"); map2.put(Constants.Keys.KEY_PHONE,"0785548754");
//
//        contacts.add(map);
//        contacts.add(map1);
//        contacts.add(map2);
//
////        SimpleAdapter adapter = new SimpleAdapter(this,contacts,android.R.layout.simple_list_item_2,from ,to);

        adapter = new SimpleAdapter(this, contacts, R.layout.list_items_layout, from, to2);


        // MyCustomAdapter myCustomAdapter = new MyCustomAdapter(getApplicationContext());
        mListView1.setAdapter(adapter);

        loadDataFromFile(FileName, contacts);
//        mListView1.setOnClickListener(this);
        mListView1.setOnItemLongClickListener(this);
        mListView1.setOnItemClickListener(this);
    }

    private ListView mListView1;
    private Button mButtonAdd;
    private EditText mEditTextInputName;
    private EditText mEditTextInputPhone;

    private void inti() {

        mSharedPreferences = getPreferences(MODE_PRIVATE);

        myDatabaseHelper = new MyDatabaseHelper(getApplicationContext());

        mListView1 = findViewById(R.id.listview1);
        mButtonAdd = findViewById(R.id.buttonAdd);
        mButtonAdd.setOnClickListener(this);
        mEditTextInputName = findViewById(R.id.editTextInputName);
        mEditTextInputPhone = findViewById(R.id.editTextInputPhone);

    }

    private void printExternalData() {
        Log.i(TAG, String.format("%-30s:%s", "Environment.getExternalStorageState()", Environment.getExternalStorageState()));
        Log.i(TAG, String.format("%-30s:%s", "Environment.getExternalStorageDirectory()", Environment.getExternalStorageDirectory()));
        Log.i(TAG, String.format("%-30s:%s", "Environment.DIRECTORY_DOCUMENTS", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)));
        Log.i(TAG, String.format("%-30s:%s", "Environment.DIRECTORY_PICTURES", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonAdd:
                String name = mEditTextInputName.getText().toString();
                String phone = mEditTextInputPhone.getText().toString();
                insertDataToList(name, phone);
                //saveToInternalFile(FileName,name,phone );
                saveToInternalFile(FileName, name, phone);
                break;
        }
    }

    private void insertDataToList(String name, String phone) {
        if (!name.isEmpty() && !phone.isEmpty()) {
            HashMap<String, String> map = new HashMap<>();
            map.put(Constants.Keys.KEY_NAME, name);
            map.put(Constants.Keys.KEY_PHONE, phone);

            // Save to SharedPreferences
            saveToSharedPreferences(String.valueOf(contacts.size()),name,phone);
            contacts.add(map);

            // Save to DB
            saveToDataBase(name,phone);

            // this will force the adapter to
            adapter.notifyDataSetChanged();

            clearTexts();

        }

    }

    private void saveToDataBase(String name, String phone) {
       long result =  myDatabaseHelper.addContact(name,phone);
       if(result >0)
        Toast.makeText(this,"Contact was add successfully",Toast.LENGTH_LONG).show();
       else
           Toast.makeText(this,"No data was added",Toast.LENGTH_LONG).show();
    }

    private void clearTexts() {
        mEditTextInputName.setText("");
        mEditTextInputPhone.setText("");
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        contacts.remove(position);
        adapter.notifyDataSetChanged();
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView mTextView = view.findViewById(R.id.textViewName);
        EditText mEditText = view.findViewById(R.id.editTextPhone);
        String text = String.format("%-20s:%s (%d) - %d", mTextView.getText().toString(), mEditText.getText().toString(), position, id);
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }


    private void saveToSharedPreferences(String key, String name, String phone) {
        SharedPreferences.Editor editor = mSharedPreferences.edit() ;
        editor.putString(key,name + ":"+phone);
        editor.apply();

    }



    private void saveToInternalFile(String filename, String name, String phone) {
        // to write to cashe files use
        // try(PrintWriter pr = new PrintWriter(new File(filename)) )
        try (PrintWriter pr = new PrintWriter(openFileOutput(filename, MODE_APPEND), true)) {
            pr.println(name + "," + phone);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), String.format("File %s is not found", filename), Toast.LENGTH_LONG).show();
        }
    }

    private void saveToCacheFile(String filename, String name, String phone) {
        try (PrintWriter pr = new PrintWriter(new File(filename))) {
            pr.println(name + "," + phone);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), String.format("File %s is not found", filename), Toast.LENGTH_LONG).show();
        }
    }


    private void loadDataFromFile(String fileName, List<Map<String, String>> data) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(openFileInput(fileName)))) {
            String line = "";
            String[] split;
            while ((line = br.readLine()) != null) {
                split = line.split(",");
                if (split != null && split.length > 1) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put(Constants.Keys.KEY_NAME, split[0]);
                    map.put(Constants.Keys.KEY_PHONE, split[1]);
                    contacts.add(map);
                }
            }

            adapter.notifyDataSetChanged();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
