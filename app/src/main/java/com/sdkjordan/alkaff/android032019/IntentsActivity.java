package com.sdkjordan.alkaff.android032019;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class IntentsActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {


    private static final int MY_CALL_PERMISSON_REQUEST_CODE = 15;
    private int My_REQ_CODE = 10;

    private Intent mCallintent ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);

        inti();

    }


    private EditText mEditTextData ;
    private Button mButtonII;
    private Button mButtonEI;
    private Button mButtonView;
    private Button mButtonPhone;
    private Button mButtonSite;
    private Button mButtonForResult;

//    private View.OnClickListener mOnClickListener ;
//    private View.OnLongClickListener mOnLongClickListener ;

    private void inti() {

        mEditTextData = findViewById(R.id.editTextData);
        mButtonII = findViewById(R.id.buttonII);
        mButtonEI = findViewById(R.id.buttonEI);
        mButtonView = findViewById(R.id.buttonView);
        mButtonPhone = findViewById(R.id.buttonViewViewPhone);
        mButtonSite = findViewById(R.id.buttonViewViewSite);
        mButtonForResult = findViewById(R.id.buttonStartForResult);

        mButtonEI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntentsActivity.this,MyNewActivity.class);
                startActivity(intent);
            }
        });

//        mOnClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId())
//                {
//                    case R.id.buttonII:
//                        startActivityWithII(); break;
//                    case R.id.buttonEI:
//                        startActivityWithEI(); break;
//                    case R.id.buttonView:
//                        startActivityWithViewAction(); break;
//                    case R.id.buttonViewViewPhone:
//                        startActivityWithViewPhone(); break;
//                    case R.id.buttonViewViewSite:
//                        startActivityWithViewSite(); break;
//                    case R.id.buttonStartForResult:
//                        startActivityAnotherForResult(); break;
//                }
//            }
//        };

//        mOnLongClickListener = new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                return false;
//            }
//        };


        actionListenersInit();
    }

    private void actionListenersInit() {


//        mButtonII.setOnClickListener(mOnClickListener);
//        mButtonII.setOnLongClickListener(mOnLongClickListener);
//        mButtonEI.setOnClickListener(mOnClickListener);
//        mButtonEI.setOnLongClickListener(mOnLongClickListener);

        mButtonII.setOnClickListener(this);
        mButtonII.setOnLongClickListener(this);
        mButtonEI.setOnClickListener(this);
        mButtonEI.setOnLongClickListener(this);
        mButtonForResult.setOnClickListener(this);
        mButtonForResult.setOnLongClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.buttonII:
                startActivityWithII(); break;
            case R.id.buttonEI:
                startActivityWithEI(); break;
            case R.id.buttonView:
                startActivityWithViewAction(); break;
            case R.id.buttonViewViewPhone:
                startActivityWithViewPhone(); break;
            case R.id.buttonViewViewSite:
                startActivityWithViewSite(); break;
            case R.id.buttonStartForResult:
                startActivityAnotherForResult(); break;

            case R.id.buttonCallPhone:
                startActivityWithCallPhone(mEditTextData.getText().toString()); break;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        switch (v.getId())
        {
            case R.id.buttonII:
                 break;
            case R.id.buttonEI:

                break;
            case R.id.buttonView:

                 break;
            case R.id.buttonViewViewPhone:
               break;
            case R.id.buttonViewViewSite:

               break;
            case R.id.buttonStartForResult:
                break;
        }
        return true;
    }


    private void startActivityAnotherForResult() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.putExtra(Constants.EXTRA_DATA,mEditTextData.getText().toString());
        startActivityForResult(intent,My_REQ_CODE);
    }

    private void startActivityWithViewPhone() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:"+mEditTextData.getText().toString()));
        startActivity(intent);
    }

    private void startActivityWithCallPhone(String phone) {
        mCallintent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phone));

        if(checkCallPermissions(Manifest.permission.CALL_PHONE))
        {
            startActivity(mCallintent);
        }
    }

    private boolean checkCallPermissions(String permission) {

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return  true ;

        if(ActivityCompat.checkSelfPermission(getApplicationContext(), permission) == PackageManager.PERMISSION_GRANTED)
            return  true ;

        else {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,permission))
            {
                Toast.makeText(getApplicationContext(),"This application need this permission to be able to make the call",Toast.LENGTH_LONG).show();
            }
            requestPermissions(new String[] {permission}, MY_CALL_PERMISSON_REQUEST_CODE);
            return  false ;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       if(requestCode == MY_CALL_PERMISSON_REQUEST_CODE)
       {
            for (int result : grantResults)
                if(result == PackageManager.PERMISSION_DENIED)
                    return;

                startActivity(mCallintent);
       }
    }

    private void startActivityWithViewSite() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+mEditTextData.getText().toString()));
        startActivity(intent);
    }

    private void startActivityWithViewAction() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        startActivity(intent);
    }

    private void startActivityWithII() {
        Intent intent = new Intent(Constants.Actions.FIRST_ACTION);
        intent.putExtra(Constants.EXTRA_DATA,mEditTextData.getText().toString());
        startActivity(intent);
    }

    private void startActivityWithEI() {
        Intent intent = new Intent(IntentsActivity.this, MyNewActivity.class);
        intent.putExtra(Constants.EXTRA_DATA,mEditTextData.getText().toString());
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
       if(requestCode == My_REQ_CODE)
       {
           switch (resultCode)
           {
               case RESULT_CANCELED:
                   mEditTextData.setText("No data from the source");
                   break;
               case RESULT_OK:
                if(data != null)
                {
                    String result = data.getStringExtra(Constants.EXTRA_RESULT);
                    mEditTextData.setText("Result:"+ result);
                }
                   break;

           }
       }
    }


}
