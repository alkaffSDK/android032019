package com.sdkjordan.alkaff.android032019;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PermissionActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int MY_CALL_PER_RQT = 100;
    private EditText mEditTextPhone ;
    private String mPhone ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEditTextPhone  =findViewById(R.id.editTextPhone);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mPhone = mEditTextPhone.getText().toString() ;

        if(hasCallPermission())
            makePhoneCall(mPhone);
    }

    private boolean hasCallPermission() {

        // Step 1 : check the API level if less than 23
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return  true ;

        //  API is 23 or higher
        // Step 2 : if API is 23 or higher, then check if the app has the permission

        if( ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
        {
            if(shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE))
                Toast.makeText(getApplicationContext(),
                        "The call permission is needed to make a phone call.",Toast.LENGTH_LONG).show();

            requestPermissions(new String[] {Manifest.permission.CALL_PHONE },MY_CALL_PER_RQT);
            return false ;
        }else return  true ;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            if(requestCode == MY_CALL_PER_RQT)
            {
                for (int i: grantResults) {
                    if(i == PackageManager.PERMISSION_DENIED)
                        return;
                }
                makePhoneCall(mPhone);
            }
    }

    @SuppressLint("MissingPermission")
    private void makePhoneCall(String phone)
    {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phone));
        startActivity(intent);
    }
}
