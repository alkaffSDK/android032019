package com.sdkjordan.alkaff.android032019;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.sdkjordan.alkaff.android032019.helpers.MyDatabaseHelper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ContactListActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEditTextName;
    private EditText mEditTextPhone;
    private ListView mListView;


    private MyDatabaseHelper myDatabaseHelper ;
    private SimpleCursorAdapter cursorAdapter  ;
    private String[] from = {MyDatabaseHelper.COLUMN_NAME, MyDatabaseHelper.COLUMN_PHONE};
    private int[] to = {R.id.textViewName , R.id.textViewMobile};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        mEditTextName = findViewById(R.id.editTextName);
        mEditTextPhone = findViewById(R.id.editTextPhone);
        mListView = findViewById(R.id.list_view_contacts);
        myDatabaseHelper = new MyDatabaseHelper(getApplicationContext()) ;


        Cursor cursor = myDatabaseHelper.getAllContacts() ;
        cursorAdapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.list_item3, cursor,from , to ,SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
                mListView.setAdapter(cursorAdapter);
        fab.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        String name = mEditTextName.getText().toString();
        String phone = mEditTextPhone.getText().toString();
        // Add contact information to the database
        myDatabaseHelper.addContact(name,phone);

        cursorAdapter.changeCursor(myDatabaseHelper.getAllContacts());
        clearText();
    }

    private void clearText() {
        mEditTextName.setText("");
        mEditTextPhone.setText("");
    }

}
