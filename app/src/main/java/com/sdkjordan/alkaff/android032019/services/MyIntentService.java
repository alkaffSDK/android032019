package com.sdkjordan.alkaff.android032019.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {

    private static final String TAG = "MyIntentService";

    private static final String ACTION_DOWNLOAD_AUDIO = "com.sdkjordan.alkaff.android032019.services.action.DOWNLOAD_AUDIO";
    private static final String ACTION_PLAY_AUDIO  = "com.sdkjordan.alkaff.android032019.services.action.DOWNLOAD_PLAY_AUDIO";
    private static final String ACTION_DELETE_FILE  = "com.sdkjordan.alkaff.android032019.services.action.DOWNLOAD_DELETE_FILE";

    private static final String EXTRA_DOWNLOAD_URL = "com.sdkjordan.alkaff.android032019.services.extra.DOWNLOAD_URL";
    private static final String EXTRA_AUDIO_LOCATION = "com.sdkjordan.alkaff.android032019.services.extra.AUDIO_LOCATION";
    private static final String EXTRA_FILE_PATH = "com.sdkjordan.alkaff.android032019.services.extra.FILE_PATH";


    public MyIntentService() {
        super("MyIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionDownload(Context context, String url, String path) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_DOWNLOAD_AUDIO);
        intent.putExtra(EXTRA_DOWNLOAD_URL, url);
        intent.putExtra(EXTRA_AUDIO_LOCATION, path);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionPlay(Context context, String path) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_PLAY_AUDIO);
        intent.putExtra(EXTRA_AUDIO_LOCATION, path);
        context.startService(intent);
    }

    public static void startDeleteFile(Context context, String filepath) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_DELETE_FILE);
        intent.putExtra(EXTRA_FILE_PATH, filepath);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DOWNLOAD_AUDIO.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_DOWNLOAD_URL);
                final String filePath = intent.getStringExtra(EXTRA_AUDIO_LOCATION);
                handleActionDownload(url, filePath);
            } else if (ACTION_PLAY_AUDIO.equals(action)) {
                final String path = intent.getStringExtra(EXTRA_AUDIO_LOCATION);
                handleActionPlay(path);
            }else if (ACTION_DELETE_FILE.equals(action))
            {
                final String filePath = intent.getStringExtra(EXTRA_FILE_PATH);
                handleActionDelete(filePath);

            }
        }
    }

    private void handleActionDelete(String filePath) {
       waitForSomeTime(1000);
        Log.d(TAG,String.format("The file will be deleted from :%s",filePath));
    }

    private void waitForSomeTime(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void handleActionDownload(String url, String path) {
        waitForSomeTime(2000);
        Log.d(TAG,String.format("Start downloading from %s to the file in path:%s",url,path));
    }

    private void handleActionPlay(String path) {
        waitForSomeTime(500);
        Log.d(TAG,String.format("Start reading file from file: %s ",path));
    }
}
