package com.sdkjordan.alkaff.android032019;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sdkjordan.alkaff.android032019.helpers.MyDatabaseHelper;

import java.util.Random;

public class ThreadingActivity extends AppCompatActivity {

    private static final long DELAY = 3000;
    private static final long PERIOD = DELAY / 100;
    private ImageView mImageView;
    private ProgressBar mProgressBar;

    private boolean mIsLoadingFlag = false;
    private MyDatabaseHelper sqlHelper ;

    private static boolean IsLoadingFlag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threading);

        sqlHelper = new MyDatabaseHelper(getApplicationContext());

        mImageView = findViewById(R.id.imageView2);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
    }

    Random random = new Random();
    private ImageLoader mImageLoader ;
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLoad:
                int id = 0 ;
                if (mIsLoadingFlag)
                    return;
                switch (random.nextInt(5))
                {
                    case 0: id = R.raw.android1 ; break;
                    case 1: id = R.raw.android2 ; break;
                    case 3: id = R.raw.android3 ; break;
                    default:
                     id = R.raw.android ;
                }
                new MyImageLoader().execute(id);
//                mProgressBar.setProgress(0);
                //loadImageWithoutThreading(mImageView);
//                loadImageWithThreading(mImageView);
//                if(mImageLoader == null || (mImageLoader != null && mImageLoader.getStatus().equals(AsyncTask.Status.FINISHED))) {
//                    mImageLoader = new ImageLoader(getApplication(), mImageView, mProgressBar);
//                    mImageLoader.execute(R.raw.android3);
//                }else {
//                    return;
//                }
                break;
            case R.id.buttonToast:
                Toast.makeText(getApplicationContext(), "Working ..", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void loadImageWithoutThreading(ImageView imageView) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.android3);
        waitForSomeTime();
        imageView.setImageBitmap(bitmap);
        mProgressBar.setProgress(0);
    }


    private void loadImageWithThreading(final ImageView imageView) {

        if (mIsLoadingFlag)
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                mIsLoadingFlag = true;
                final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.android3);
                waitForSomeTime();

                // the next line will throw an exception : Only the original thread that created a view hierarchy can touch its views.
                // imageView.setImageBitmap(bitmap);
                // To solve this error we have 2 options

                // Option 1: using View.post method
//                imageView.post(new Runnable() {         // this will run in the Main UI thread
//                    @Override
//                    public void run() {
//                        imageView.setImageBitmap(bitmap);
//                    }
//                });

                // Option 2: using  runOnUiThread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);
                    }
                });

                mProgressBar.setProgress(0);
                mIsLoadingFlag = false;
            }
        }).start();


    }

    private void waitForSomeTime() {
        try {
            for (int i = 1; i <= 100; i++) {
                Thread.sleep(PERIOD);
                mProgressBar.setProgress(i);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // It is recommended to make the AsyncTask class static (see ImageLoader class for details)
    private class MyImageLoader extends  AsyncTask<Integer,Integer,Bitmap>
    {


        // This method will run on the Main UI thread
        @Override
        protected void onPreExecute() {
            mIsLoadingFlag = true;

            mProgressBar.setProgress(0);
            super.onPreExecute();
        }

        // This method will run on the Worker thread
        @Override
        protected Bitmap doInBackground(Integer... integers) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), integers[0]);
            for(int i=1;i<= 100;i++)
            {
                try {
                    Thread.sleep(100);
                    publishProgress(i);                  // Will run in the Main UI Thread and will call onProgressUpdate

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return bitmap;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            mProgressBar.setProgress(values[0]);
        }


        // This method will run on the Main UI thread
        // The output of the doInBackground method will be send to onPostExecute
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            mImageView.setImageBitmap(bitmap);
            mIsLoadingFlag = false;
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            mImageView.setImageBitmap(bitmap);
        }

        @Override
        protected void onCancelled() {
            mImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),R.raw.android));
        }


    }

    private static class ImageLoader extends AsyncTask<Integer, Integer, Bitmap> {
        private ImageView imageView;
        private Context context;
        private ProgressBar progressBar;

        public ImageLoader(Context c, ImageView iv, @Nullable ProgressBar pb) {
            super();
            imageView = iv;
            context = c;
            progressBar = pb;

        }

        @Override
        protected void onPreExecute() {     // Runs before doInBackground on the main UI thread
            super.onPreExecute();
            if (progressBar != null) {
                progressBar.setMax(100);
                progressBar.setProgress(0);
            }
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {               // Run on the worker thread
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), integers[0]);
            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(PERIOD);
                    publishProgress(i);      // will call onProgressUpdate
                }
                return bitmap;
            } catch (InterruptedException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {            //// Runs after doInBackground on the main UI thread
            if (bitmap != null)
                imageView.setImageBitmap(bitmap);

            if (progressBar != null)
                progressBar.setProgress(0);
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            Toast.makeText(context,"Task was cancelled", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(context,"Task was cancelled", Toast.LENGTH_SHORT).show();
        }

    }

}
