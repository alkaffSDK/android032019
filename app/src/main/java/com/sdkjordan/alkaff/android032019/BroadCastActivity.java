package com.sdkjordan.alkaff.android032019;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sdkjordan.alkaff.android032019.receivers.DynamicReceiver;

import java.util.Locale;
import java.util.logging.Level;


public class BroadCastActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {


    private Button mButtonSend;
    private TextView mTextViewCharging;
    private EditText mEditTextData;
    private Switch mASwitch;

    DynamicReceiver mDynamicReceiver;
    PowerConnectionReceiver powerConnectionReceiver;
    IntentFilter mIntentFilter;
    IntentFilter mPowerIntentFilter;

    LinearLayout mLayout;

    TextView textViewAction, textViewStatus, textViewLevel, textViewPer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broad_cast);


        mButtonSend = findViewById(R.id.buttonSendBC);
        mEditTextData = findViewById(R.id.editTextData1);
        mASwitch = findViewById(R.id.switchRciver1);
        mLayout = findViewById(R.id.ll1);
        //mTextViewCharging = findViewById(R.id.textViewCharging) ;

        mButtonSend.setOnClickListener(this);
        mASwitch.setOnCheckedChangeListener(this);


        mIntentFilter = DynamicReceiver.getIntentFilter();
        mDynamicReceiver = new DynamicReceiver();

        powerConnectionReceiver = new PowerConnectionReceiver();

        mPowerIntentFilter = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
        mPowerIntentFilter.addAction(Intent.ACTION_BATTERY_LOW);
        mPowerIntentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
        mPowerIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        mPowerIntentFilter.addAction(Intent.ACTION_POWER_CONNECTED);
        mPowerIntentFilter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        mPowerIntentFilter.setPriority(900);


        textViewAction = new TextView(this);
        textViewStatus = new TextView(this);
        textViewPer = new TextView(this);
        textViewLevel = new TextView(this);

        mLayout.addView(textViewAction);
        mLayout.addView(textViewStatus);
        mLayout.addView(textViewPer);
        mLayout.addView(textViewLevel);


    }

    @Override
    public void onClick(View v) {

        Intent intent = new Intent(Constants.Actions.MY_ACTION);
        intent.putExtra(Constants.EXTRA_DATA, mEditTextData.getText().toString());
        mEditTextData.setText("");
        sendBroadcast(intent);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked)
            registerReceiver(mDynamicReceiver, mIntentFilter);
        else
            unregisterReceiver(mDynamicReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDynamicReceiver != null)
            unregisterReceiver(mDynamicReceiver);

        if (powerConnectionReceiver != null)
            unregisterReceiver(powerConnectionReceiver);
    }

    @Override
    protected void onResume() {
        if (powerConnectionReceiver != null)
            registerReceiver(powerConnectionReceiver, mPowerIntentFilter);

        if (mASwitch.isChecked())
            registerReceiver(mDynamicReceiver, mIntentFilter);
        super.onResume();


    }

    public class PowerConnectionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String action = null, text = null, value = null, statusTest = null;
            action = intent.getAction();

            int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

            switch (status) {
                case BatteryManager.BATTERY_STATUS_CHARGING:
                    statusTest = "Charging";
                    break;
                case BatteryManager.BATTERY_STATUS_FULL:
                    statusTest = "Full";
                    break;
                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                    statusTest = "Discharging";
                    break;
                case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                    statusTest = "Not charging";
                    break;
                case BatteryManager.BATTERY_STATUS_UNKNOWN:
                    statusTest = "Unknown";
                    break;
            }


            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

            float batteryPct = level / (float) scale;


            switch (action) {
                case Intent.ACTION_POWER_CONNECTED:

                    text = "Power Connected";

                    break;
                case Intent.ACTION_POWER_DISCONNECTED:
                    text = "Power Disconnected Connected";
                    break;

            }
            textViewAction.setText(String.format(Locale.getDefault(), "%-20s : %s", "Action", action));
            textViewStatus.setText(String.format(Locale.getDefault(), "%-20s : %s", "Status", statusTest));
            textViewLevel.setText(String.format(Locale.getDefault(), "%-20s : %s", "Level", Integer.valueOf(level)));
            textViewPer.setText(String.format(Locale.getDefault(), "%-20s : %s%%", "Percent", Float.valueOf(batteryPct)));

        }
    }

}
