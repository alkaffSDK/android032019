package com.sdkjordan.alkaff.android032019;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.NotificationCompat;

import com.google.android.material.snackbar.Snackbar;

public class UserNotificationActivity extends AppCompatActivity implements View.OnClickListener, AlertDialog.OnClickListener {

    private static final String CHANNEL1_ID = "channel_1";
    private static final String CHANNEL2_ID = "channel_2";

    Activity mActivity;

    private EditText mEditTextData;

    private String[] countries;

    private AlertDialog mExitAlertDialog;
    private AlertDialog mDataAlertDialog;
    private AlertDialog mSingleChoiceDialog;
    private AlertDialog mMultiChoiceDialog;

    private static final int MY_PENNDING_REQUEST = 10;

    private boolean isBackpressed = false;


    Notification notification;
    NotificationManager notificationManager;

    private PendingIntent mpendingItent;
    private int mNotificationId = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notification);

        mActivity = this;
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel();

        Intent intent = new Intent(Constants.Actions.MY_ACTION);
        mpendingItent = PendingIntent.getActivity(getApplicationContext(), MY_PENNDING_REQUEST, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        inti();
    }

    private void inti() {
        countries = getResources().getStringArray(R.array.countries);
        checkedItems = new boolean[countries.length];
        mEditTextData = findViewById(R.id.editTextData1);
    }

    public void onClick(View view) {


        String data = mEditTextData.getText().toString();
        mEditTextData.setText("");
//        if (data.isEmpty())
//            return;

        switch (view.getId()) {
            case R.id.buttonToast:
                showToast(data);
                break;
            case R.id.buttonCustomToast:
                showCustomToast(data);
                break;
            case R.id.buttonInputDialog:
                getDataFromDialog();
                break;
            case R.id.buttonSingleChoiceDialog:
                showSingleChoiceDialog();
                break;
            case R.id.buttonMultiDialog:
                showMultiChoiceDialog();
                break;
            case R.id.buttonPopUp:
                createPopUpMenu(view);
                break;

            case R.id.buttonNotification:
                if (notification == null) {
                    notification = createNotification(data);
                } else {
                    updateNotification(data);
                }

                notificationManager.notify(mNotificationId, notification);
                break;

            case R.id.buttonScackBar:
                showSnackBar(view, "this is a snackBar", "dismiss", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(),"snackBar will be dismissed", Toast.LENGTH_SHORT).show();

                    }
                });
                break;
        }
    }


    EditText mEditTextInput;
    View view   ;
    private void getDataFromDialog() {
        if(mEditTextInput == null)
        {
            view  = getLayoutInflater().inflate(R.layout.dialog_input_layout,null);
            mEditTextInput = view.findViewById(R.id.editTextInputData);
        }
        if (mDataAlertDialog == null) {


            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setCancelable(false)
                    .setTitle(R.string.txt_data)
                    .setView(view)
                    .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mEditTextData.setText(mEditTextInput.getText());
                        }
                    })
                    .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            mDataAlertDialog = builder.create() ;
        }
        mEditTextInput.setText(mEditTextData.getText());
        mDataAlertDialog.show();
    }


    private void showToast(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        // OR

//        Toast toast1 = new Toast(getApplicationContext());
//        toast1.setDuration(Toast.LENGTH_SHORT);
//        toast1.setText(msg);
//        toast1.setGravity(Gravity.CENTER, 0, 0);
//        toast1.show();
    }

    private void showCustomToast(String msg) {
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
//
//        // Inflate the toast layout
        View view = getLayoutInflater().inflate(R.layout.toast_layout, null);
//        // get the text view from the toast view
        TextView textView = view.findViewById(R.id.textViewDataInToast);
        textView.setText(msg);

        // set the view for the toast
        toast.setView(view);
        toast.show();
    }


    private void showExitConfirmationDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);            // AlertDialog Builder takes the activity context NOT ApplicationContext

       // builder.setTitle("").setMessage("").setPositiveButton("",this).show()

//        builder.setTitle(getResources().getString(R.string.txt_exit))
//                .setMessage(getResources().getString(R.string.txt_exit_msg))
//                .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        mActivity.finish();
//                        // or
//                        // UserNotificationActivity.super.onBackPressed();
//                    }
//                })
//                .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        dialogInterface.dismiss();
//                    }
//                })
//                .setCancelable(true)
//                .show();


        if (mExitAlertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.txt_exit))
                    .setMessage(getResources().getString(R.string.txt_exit_msg))
                    .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mActivity.finish();
                            // or
                            // UserNotificationActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setCancelable(true);

            mExitAlertDialog = builder.create();
        }

        mExitAlertDialog.show();
    }


    int selectedItem = -1;

    private void showSingleChoiceDialog() {

       if(mSingleChoiceDialog == null)
       {
           AlertDialog.Builder builder = new AlertDialog.Builder(this);
           builder.setTitle(R.string.txt_single_choice)
                   .setSingleChoiceItems(countries, selectedItem, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int id) {
                           selectedItem = id ;
                       }
                   })
                   .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {
                           mEditTextData.setText(countries[selectedItem]);
                       }
                   })
                   .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {
                           dialogInterface.dismiss();
                       }
                   })
                   .setCancelable(false);

           mSingleChoiceDialog = builder.create() ;
       }
       mSingleChoiceDialog.show();
    }

    private boolean[] checkedItems;
    private void showMultiChoiceDialog() {

        if(mMultiChoiceDialog == null)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.txt_single_choice)
                    .setMultiChoiceItems(countries, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int id, boolean selected) {
                            checkedItems[id] = selected ;
                        }
                    })
                    .setPositiveButton(R.string.txt_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int id) {
                            StringBuilder builder = new StringBuilder();
                            for (int i = 0; i < checkedItems.length; i++) {
                                if (checkedItems[i]) {
                                    builder.append(countries[i] + ", ");
                                }
                            }
                            mEditTextData.setText(builder.substring(0,builder.length()-2));
                        }
                    })
                    .setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .setCancelable(false);
            mMultiChoiceDialog = builder.create() ;
        }
        mMultiChoiceDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case AlertDialog.BUTTON_POSITIVE:
                if (dialog.equals(mExitAlertDialog))
                    super.onBackPressed();
                else if (dialog.equals(mDataAlertDialog)) {
                    mEditTextData.setText(mEditTextInput.getText());
                } else if (dialog.equals(mSingleChoiceDialog)) {
                    if (selectedItem > -1)
                        mEditTextData.setText(countries[selectedItem]);
                } else if (dialog.equals(mMultiChoiceDialog)) {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < checkedItems.length; i++) {
                        if (checkedItems[i]) {
                            builder.append(countries[i] + " /");
                        }
                    }
                    mEditTextData.setText(builder.toString());
                }
                break;
            case AlertDialog.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
            case AlertDialog.BUTTON_NEUTRAL:
                break;
        }
    }

    private void createPopUpMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
         inflater.inflate(R.menu.popup_items, popup.getMenu());

         popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
             @Override
             public boolean onMenuItemClick(MenuItem item) {
                 Toast.makeText(getApplicationContext(),item.getTitle(), Toast.LENGTH_SHORT).show();

                 return true;
             }
         });
        popup.show();
    }

    @Override
    public void onBackPressed() {
        showExitConfirmationDialog();

    }
    private void showSnackBar(@NonNull View v, @NonNull String text, @Nullable String action, @Nullable View.OnClickListener listener) {
        Snackbar mySnackbar = Snackbar.make(v, text, Snackbar.LENGTH_INDEFINITE);
        if (action != null)
            mySnackbar.setAction(action, listener);
        mySnackbar.show();

    }
    NotificationCompat.Builder builder;
    private Notification createNotification(String text) {

        NotificationCompat.Action actionPlay = new NotificationCompat.Action(R.drawable.ic_notification_play,"Play",mpendingItent);
        NotificationCompat.Action actionDismiss = new NotificationCompat.Action(R.drawable.ic_notification_stop,"Stop",mpendingItent);

        if (builder == null) {
            builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL1_ID);
            builder.setSmallIcon(android.R.drawable.ic_dialog_alert)
                    .setContentTitle("Notification")
                   //.setContentIntent(mpendingItent)
                    .addAction(actionPlay)
                    .addAction(actionDismiss)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentText(text);
        }
        return builder.build();
    }
    private void updateNotification(String text) {
        if (builder == null)
            notification = createNotification(text);

        else {
            builder.setContentText(text);
            notification = builder.build();
        }
    }
    private void createNotificationChannel() {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if(notificationManager == null) notificationManager = getSystemService(NotificationManager.class);
            if (null == notificationManager.getNotificationChannel(CHANNEL1_ID)) {
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL1_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
               // notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }

            if (null == notificationManager.getNotificationChannel(CHANNEL2_ID)) {
                CharSequence name = getString(R.string.channel2_name);
                String description = getString(R.string.channel2_description);
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel channel = new NotificationChannel(CHANNEL2_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                // notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}
