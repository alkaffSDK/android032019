package com.sdkjordan.alkaff.android032019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sdkjordan.alkaff.android032019.services.FirstService;
import com.sdkjordan.alkaff.android032019.services.MyIntentService;
import com.sdkjordan.alkaff.android032019.services.SecondIntentService;

public class ServiceActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private EditText mEditText  , mEditTextURL, mEditTextPath;
    private Spinner mSpinner ;
    private static final String DOWNLOAD_ACTION = "Download" ;
    private static final String PLAY_ACTION = "Play" ;
    private static final String DELETE_ACTION = "Delete" ;

    private String[] actions = {"",DOWNLOAD_ACTION ,PLAY_ACTION,DELETE_ACTION};;

    String mSelectAction  = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        actions[0] = getResources().getString(R.string.please_select);
        mEditText = findViewById(R.id.editTextServiceData);

        mEditTextURL= findViewById(R.id.editTextURL);
        mEditTextPath = findViewById(R.id.editTextPath);


        mSpinner = findViewById(R.id.spinner);
        mSpinner.setAdapter(new ArrayAdapter<String>(ServiceActivity.this,R.layout.item_list,R.id.textViewItem,actions));
        mSpinner.setOnItemSelectedListener(this);

    }

    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.buttonStartFirstService:
                startFirstService(); break;

            case R.id.buttonStartSecondService:
                startSecondService(); break;

            case R.id.buttonStart:
                startIntentService(); break;
        }

        mEditText.setText("");
    }

    private void startIntentService() {

        String url = mEditTextURL.getText().toString();
        String path = mEditTextPath.getText().toString();
        switch (mSelectAction)
        {
            case DOWNLOAD_ACTION:
                MyIntentService.startActionDownload(ServiceActivity.this,url,path);
                break;
            case PLAY_ACTION:
                MyIntentService.startActionPlay(ServiceActivity.this,path);
                break;
            case DELETE_ACTION:
                MyIntentService.startDeleteFile(ServiceActivity.this,path);
                break;
                default:
                    Toast.makeText(ServiceActivity.this,"No action was selected",Toast.LENGTH_LONG).show();

        }
        mSpinner.setSelection(0);
    }

    private void startSecondService() {
        Intent intent = new Intent(ServiceActivity.this, SecondIntentService.class);
        intent.putExtra(Constants.EXTRA_DATA,mEditText.getText().toString());
        startService(intent);
    }

    private void startFirstService() {
        Intent intent = new Intent(ServiceActivity.this, FirstService.class);
        intent.putExtra(Constants.EXTRA_DATA,mEditText.getText().toString());
        startService(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        mSelectAction = actions[i];
        if(mSelectAction.equals(DOWNLOAD_ACTION))
        {
            mEditTextURL.setVisibility(View.VISIBLE);
        }else{
            mEditTextURL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mSelectAction = null ;
    }
}
