package com.sdkjordan.alkaff.android032019.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDatabaseHelper extends SQLiteOpenHelper {


    private static final int VERSION = 3 ;
    private static final String DATABASE_NAME = "contacts_db" ;
    public static final String TABLE_CONTACTS = "contacts" ;
    public static final String COLUMN_ID = "_id" ;
    public static final String COLUMN_NAME = "name" ;
    public static final String COLUMN_PHONE = "phone" ;


    public static final String CREATE_CONTACT_TABLE  =
            String.format("CREATE TABLE %s ( %s INTEGER AUTOINCREAMENT PRIMARY KEY ," +
                            " %s TEXT NOT NULL , %s TEXT NOT NULL UNIQUE );"
                    ,TABLE_CONTACTS,COLUMN_ID,COLUMN_NAME ,COLUMN_PHONE);


    // This is another way to write the command without using String.format method
    public static final String CREATE_CONTACT_TABLE1  =
            "CREATE TABLE "+TABLE_CONTACTS+" ( "+COLUMN_ID+" INTEGER AUTOINCREAMENT PRIMARY KEY ," +
                    COLUMN_NAME+ "  TEXT NOT NULL , "+COLUMN_PHONE+"%s TEXT NOT NULL UNIQUE );";

    public MyDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_CONTACT_TABLE);

    }

    public long addContact(String name, String phone)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME,name);
        values.put(COLUMN_PHONE,phone);
        return getWritableDatabase().insert(TABLE_CONTACTS,null,values);
    }


    public Cursor getAllContacts(){
        return  getReadableDatabase().query(TABLE_CONTACTS,null,null,null,null,null,null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // TODO : check versions difference and migrate the old data to the new one
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
        // TODO : check versions difference and migrate the new version  data to the old one
    }
}
