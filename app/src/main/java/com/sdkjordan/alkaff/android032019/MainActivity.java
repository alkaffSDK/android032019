package com.sdkjordan.alkaff.android032019;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG =  "android032019";
    private static final String ACTIVITY = "MainActivity";

    TextView mTextView ;
    EditText mEditText ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                // this will inflate R.layout.activity_main into objects
        setContentView(R.layout.activity_main);

        Log.d(TAG,ACTIVITY +  ": onCreate()");
        mTextView = findViewById(R.id.my_text_view);
        mTextView.setText(Integer.toHexString(R.id.my_text_view));

        mEditText = findViewById(R.id.editText) ;
        mEditText.setText("Changed");


//        TextView textView = new TextView(this);
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT,1.0F);
//        textView.setLayoutParams(layoutParams);
//        textView.setText("Hello World");

    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,ACTIVITY +  ": onStart()");

    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,ACTIVITY +  ": onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,ACTIVITY +  ": onDestroy()");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,ACTIVITY +  ": onBackPressed()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,ACTIVITY +  ": onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,ACTIVITY +  ": onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,ACTIVITY +  ": onRestart()");
    }

    public void doAction(View v)
    {
        startActivity(new Intent(MainActivity.this,MyNewActivity.class));
        mTextView.setText(mEditText.getText());
    }

}
