package com.sdkjordan.a.alkaff.agecalculator;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.LocaleData;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements  View.OnFocusChangeListener {

     EditText mEditTextDate ;
    EditText mEditTextName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEditTextName = findViewById(R.id.editTextName);
        mEditTextDate = findViewById(R.id.editTextDate);
        mEditTextDate.setOnFocusChangeListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment(v);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private int  currentYear , currentMonth,currentDay ;

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b)
            showDatePickerDialog(mEditTextDate);
    }

    private String calculateAge(Date current, Date birth)
    {
        long diff = current.getTime() - birth.getTime(); //this is going to give you the difference in milliseconds

        Date result = new Date(diff);
        Format frmt = new SimpleDateFormat("yy MM dd");
        return frmt.format(result);
    }

    private String calculateAge1(int cYear, int cMonth, int cDay, int bYear, int bMonth, int bDay)
    {
        return null ;
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        View mView ;
        public DatePickerFragment(View v) {
            mView = v ;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int currentYear = c.get(Calendar.YEAR);
            int currentMonth = c.get(Calendar.MONTH);
            int currentDay = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, currentYear, currentMonth, currentDay);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            if(mView instanceof TextView) {
                ((TextView) mView).setText(String.format("%02d/%02d/%04d", day, month, year));


            }
        }
    }
}
